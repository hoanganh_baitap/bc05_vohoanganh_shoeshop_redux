import logo from './logo.svg';
import './App.css';
import BaiTapGioHangShoe from './BaiTapGioHangShoes/BaiTapGioHangShoe';

function App() {
  return (
    <div className="container py-5">
      <BaiTapGioHangShoe />
    </div>
  );
}

export default App;
