// import data shoes
import { TANG_GIAM, THEM_GIO_HANG, THONG_TIN, XOA_GIO_HANG } from "../constant";
import {data} from "../data/data"



let storeState = {
    dataShoe: data,
    store: [],
    detail: data[0]
}



const storeReducer = (state = storeState, action)=> {
    switch(action.type){
        case THEM_GIO_HANG: {
            // tao 1 gio hang moi
            let cloneStore = [...state.store];
            // tim va so sanh id trong gio hang moi duoc tao ra voi id trong sp dc click vao
            let index=cloneStore.findIndex(sp=>sp.id===action.sp.id);
            // neu tim thay thi push sp do vao mang, khong tim thay thi tang so luong ben trong mang ban dau len 1
            if(index !== -1){
                cloneStore[index].soLuong+=1;
            }else{
                // tao ra 1 bien chua gio hang voi them 1 thuoc tinh la so luong
                let gioHangMoi = {...action.sp, soLuong: 1}
                cloneStore.push(gioHangMoi);
            }
            // set lai state
            state.store = cloneStore;
            return {...state}
        };break;
        case XOA_GIO_HANG: {
            // tao 1 gio hang moi
            let cloneStore = [...state.store];
            // tim id cua san pham can xoa voi id cua san pham trong gio hang
            let index = cloneStore.findIndex(sp=>sp.id === action.id);
            // neu tim thay thi xoa san pham tai vi tri do
            if(index !== -1){
                cloneStore.splice(index, 1);
            }
            // set lai state
            state.store = cloneStore;
            return {...state}
        }; break;
        case TANG_GIAM: {
            // tao 1 gio hang moi
            let cloneStore = [...state.store];
            // tim id san pham trong gi hang voi id cua action
            let index = cloneStore.findIndex(sp=>sp.id === action.id);
            // neu value = true thi cho soLuong +=1, false thi soLuong -= 1;
            if(action.value=== true){
                cloneStore[index].soLuong +=1;
            }
            if(action.value === false){
                if(cloneStore[index].soLuong > 1){
                    cloneStore[index].soLuong -=1;
                }
            }
            // set lai state gio hang
            state.store = cloneStore;
            return {...state}
        };break;
        case THONG_TIN: {
            state.detail = action.payload;
            return {...state}
        }


        default: return state

    }
    return state


}

export default storeReducer;