import { combineReducers } from "redux";
import storeReducer from "./storeReducer";



const rootReducer = combineReducers({
    storeReducer : storeReducer
});
export default rootReducer;