import React, { Component } from "react";
import DanhSachSanPham from "./DanhSachSanPham";
import GioHang from "./GioHang";
import { connect } from "react-redux";
import ThongTinSanPham from "./ThongTinSanPham";


class BaiTapGioHangShoe extends Component {
  render() {
    return (
      <div>
        <h1 className="text-center">Shoes</h1>
        <div className="text-right">
        <button
          type="button"
          className="btn btn-primary btn-lg"
          data-toggle="modal"
          data-target="#modelId"
        >
          Giỏ hàng ({this.props.gioHang.reduce((sl, sp, index)=>{
            return sl+=sp.soLuong
          }, 0)})
        </button>
        </div>

        <GioHang />
        <DanhSachSanPham />
        <ThongTinSanPham />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    gioHang: state.storeReducer.store,
  };
};

export default connect(mapStateToProps)(BaiTapGioHangShoe);
