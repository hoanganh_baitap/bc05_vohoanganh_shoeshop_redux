import React, { Component } from 'react'
import { connect } from 'react-redux'

class ThongTinSanPham extends Component {
  render() {
    return (
      <div className='row bg-primary text-light p-5' style={{}}>
        <div className="col-6">
            <img src={this.props.detail.image} alt="" />
        </div>
        <div className="col-6" style={{fontSize: 20}}>
            <p>{this.props.detail.name}</p>
            <p>{this.props.detail.price}</p>
            <p>{this.props.detail.description}</p>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state)=>{
    return {
        detail: state.storeReducer.detail
    }
}

export default connect(mapStateToProps)(ThongTinSanPham)

