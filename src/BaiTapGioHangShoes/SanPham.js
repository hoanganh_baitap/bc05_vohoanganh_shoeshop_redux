import React, { Component } from "react";
import { connect } from "react-redux";
import { THEM_GIO_HANG, THONG_TIN } from "./constant";

class SanPham extends Component {
  render() {
    let {sp} = this.props;
    return (
      <div className="card" style={{height: 400}}>
        <img className="card-img-top" src={sp.image} style={{width: 220, height: 220, padding:20}} alt ="" />
        <div className="card-body">
          <h6 className="card-title">{sp.name}</h6>
        </div>
        <div className="card-footer">
            <button onClick={()=>this.props.themSpGioHang(sp)} className="btn btn-danger">Buy</button>
            <button onClick={()=>this.props.xemThongTin(sp)} className="btn btn-primary ml-2">Detail</button>
        </div>
      </div>
    );
  }
}

// viet ham gui du lieu len redux xu ly
const mapDispatchToProps = (dispatch)=>{
  return{
    themSpGioHang: (sp)=>{
      let action = {
        type: THEM_GIO_HANG,
        sp
      }
      dispatch(action)
    },
    xemThongTin: (sp)=>{
      let action = {
        type: THONG_TIN,
        payload: sp
      }
      dispatch(action)
    }
  }
}

export default connect(null,mapDispatchToProps)(SanPham)
