import React, { Component } from "react";
// import thu vien connect
import { connect } from "react-redux";
import { TANG_GIAM, XOA_GIO_HANG } from "./constant";

class GioHang extends Component {
  // lay du lieu tu gio hang tren redux render ra tai table gio hang
  renderTable = ()=>{
    return this.props.gioHang.map((sp, index)=>{
      return <tr key={index}>
        <td>{sp.id}</td>
        <td>{sp.name}</td>
        <td><img src={sp.image} style={{width: 100, height: 100}} alt="" /></td>
        <td>{sp.price}</td>
        <td>
          <button onClick={()=>this.props.handleTangGiam(sp.id, true)} className="btn btn-warning mr-1">+</button>
          {sp.soLuong}
          <button onClick={()=>this.props.handleTangGiam(sp.id, false)} className="btn btn-warning ml-1">-</button>
        </td>
        <td>{sp.price * sp.soLuong}</td>
        <td>
          <button onClick={()=>this.props.xoaSpGioHang(sp.id)} className="btn btn-danger">Xóa</button>
        </td>
      </tr>


    })
  }
  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{width: 1000}}>
            <div className="modal-header">
              <h5 className="modal-title">Giỏ hàng của bạn</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Mã sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>Hình ảnh</th>
                            <th>Đơn giá</th>
                            <th>Số lượng</th>
                            <th>Thành tiền</th>
                            <th>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                      {this.renderTable()}
                    </tbody>
                    <tfoot>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Tổng tiền:</th>
                      <th>{this.props.gioHang.reduce((tongTien, sp, index)=>{
                        return tongTien+= sp.soLuong * sp.price
                      }, 0)}</th>

                    </tfoot>
                </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// viet ham lay thong tin tu redux
const mapStateToProps = (state)=>{
  return {
    gioHang: state.storeReducer.store
  }
}
// viet ham gui thong tin len redux de xu ly
const mapDispatchToProps = (dispatch)=>{
  return {
    xoaSpGioHang: (id)=>{
      let action = {
        type: XOA_GIO_HANG,
        id
      }
      dispatch(action)
    },
    handleTangGiam: (id, value)=>{
      let action ={
        type: TANG_GIAM,
        id, value
      }
      dispatch(action)
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(GioHang);