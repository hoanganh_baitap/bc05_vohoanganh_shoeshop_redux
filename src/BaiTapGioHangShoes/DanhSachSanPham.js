import React, { Component } from 'react'
// import thu vien ket noi toi store o reducer
import { connect } from 'react-redux'
import SanPham from './SanPham'

class DanhSachSanPham extends Component {
    // viet ham render danh sach san pham o day
    renderDanhSach = ()=>{
        return this.props.listShoe.map((sp, index)=>{
            return <div key={index} className='col-3 mb-4 h-400 mt-5'>
                <SanPham sp={sp}/>
            </div>
        })
    }
    
  render() {
    return (
      <div className='row mx-auto'>
        {this.renderDanhSach()}
      </div>
    )
  }
}

// viet ham lay du lieu tu store reducer
const mapStateToProps = (state)=>{
    // return ve 1 objec de co cap doi tuong key value
    return {
        listShoe: state.storeReducer.dataShoe
    }
}


export default connect(mapStateToProps)(DanhSachSanPham)
